# Ansible GitlabCI Image
[![pipeline status](https://gitlab.com/diffy0712/ansible-gitlabci-image/badges/main/pipeline.svg?ignore_skipped=true)](https://gitlab.com/diffy0712/ansible-ansible-gitlabci-image/-/commits/main)

- [Ansible GitlabCI Image](#ansible-gitlabci-image)
  - [Introduction](#introduction)
  - [Intent](#intent)
  - [Example .gitlab-ci.yml](#example-gitlab-ciyml)

## Introduction
A Docker image building on `docker:stable-dind`.

> NOTE: I am not planning to maintain versions of this. Just simply use latest and sometime rebuild so it uses the latest base image.

## Intent
Running setup task for the pipeline as a `before_script` action was really slow. Installing the dependencies took a lot of time, so I decided to try to build an already prepared image and use it instead in my pipelines.

## Example .gitlab-ci.yml

```
---
image: registry.gitlab.com/diffy0712/ansible-gitlabci-image:latest

services:
  - docker:dind

include:
  - local: '.gitlab-ci.test.yml'
```